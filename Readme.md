# Calculator project
 This project is the replica of a calculator Which can be used to calculate the value by using operators.
 Teck stack-: HTMl, CSS, JavaScript

 # What this application do?

Its primarily work is to take the numbers and operator as an input and return the calculated value.

# Used technologies
 
 Html and CSS to make the structure of the calculator and how it looks.
 JavaScript to give the functionality to the calculator to work properly. 

 # The challenges

 * To create the boxes and align them together 
 * Taking the numbers as a string so can take the value more than one digit number

 # Changes which I will try to make it in future
 * In future will try to make it as a scientific calculator

 # Project Link 
 https://classy-praline-56f951.netlify.app/



